module.exports = function (app) {
    var film = require('../controllers/filmController')

    // Film Routes
    app.route('/film')
        .get(film.read)
        .post(film.create)

    app.route('/film/:filmId')
        .get(film.readById)
        .put(film.update)
        .delete(film.delete)
}