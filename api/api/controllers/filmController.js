var mongoose = require('mongoose'),
    Film = mongoose.model('Film')

exports.create = function(req, res){
    var newFilm = new Film(req.body)

    newFilm.save(function (err, film) {
        if (err) {
            res.send(err)
        } else {
            res.json(film)
        }
    })
}

exports.read = function(req, res) {
    Film.find({}, function (err, film) {
        if (err) {
            res.send(err)
        } else {
            res.json(film)
        }
    })
}

exports.update = function(req, res) {
    Film.findOneAndUpdate({_id: req.params.filmId}, req.body, {new: true}, function (err, film) {
        if (err) {
            res.send(err)
        } else {
            res.json(film)
        }
    })
}

exports.delete = function(req, res) {
    Film.remove({_id: req.params.filmId}, function(err, film) {
        if (err) {
            res.send(err)
        } else {
            res.json({message: 'Film Successfuly Delete'})
        }
    })
}

exports.readById = function (req, res) {
    Film.findById(req.params.filmId, function(err, film) {
        if (err) {
            res.send(err)
        } else {
            res.json(film)
        }
    })
}