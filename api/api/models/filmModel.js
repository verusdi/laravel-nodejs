var mongoose = require('mongoose'),
    Schema = mongoose.Schema

var FilmSchema = new Schema({
    judul: String,
    tayang: String,
    bioskop: String,
    sinopsis: String,
    harga: String,
    image: String,
    trailer: String,
})

module.exports = mongoose.model('Film',FilmSchema)