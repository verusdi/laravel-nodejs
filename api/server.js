var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    mongoose = require('mongoose'),
    Film = require('./api/models/filmModel'),
    bodyParser = require('body-parser')


mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost/film',{ useNewUrlParser: true })

app.use(bodyParser.urlencoded({ extended:true }))
app.use(bodyParser.json())

var routes = require('./api/routes/filmRoutes')
routes(app)

app.listen(port)

console.log('Server listen to : ' + port)

